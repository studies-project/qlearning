#include "Island.hpp"

Island::Island(int sizeX, int sizeY, int numberBottleRhum) {
	islandVector = std::vector<std::vector<int>> (sizeX, std::vector<int>(sizeY, 0));

	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distributionX(0, sizeX-1);
	std::uniform_int_distribution<int> distributionY(0, sizeY-1);

	// For all the bottle of rhum we verify if the value is not already taken
	int i=0;
	while (i<numberBottleRhum) {
		std::pair<int, int> coordinate (distributionX(generator), distributionY(generator));
		if (islandVector.at(coordinate.first).at(coordinate.second) == 0) {
			islandVector.at(coordinate.first).at(coordinate.second) = 2;
			i++;
		}
	}

	// For the treasure we search coordinates not taken
	std::pair<int, int> coordinateTreasure (distributionX(generator), distributionY(generator));
	while (islandVector.at(coordinateTreasure.first).at(coordinateTreasure.second) != 0)
		coordinateTreasure = std::make_pair(distributionX(generator), distributionY(generator));
	islandVector.at(coordinateTreasure.first).at(coordinateTreasure.second) = 10;
}

void Island::printIsland() {
	for (unsigned i=0; i<islandVector.size(); i++) {
		for (unsigned j=0; j<islandVector.at(i).size(); j++) {
			if (islandVector.at(i).at(j) < 10)
				std::cout<<" ";
			// Addition of colors for easier readability
			switch (islandVector.at(i).at(j)) {
				case 2:
					std::cout<<Color::Modifier(Color::colorRed);
					break;

				case 10:
					std::cout<<Color::Modifier(Color::colorGreen);
					break;
				
				default:
					std::cout<<Color::Modifier(Color::colorDarkGray);
					break;
			}
			std::cout<<islandVector.at(i).at(j)<<" "<<Color::Modifier(Color::colorDefault);
		}
		std::cout<<std::endl;
	}
}


std::vector<std::pair<int, int>> Island::possibleMoves(int i, int y) {
	// Listing all possible moves in a 4 vector to get all directions and make easier the construction of the matrix.
	std::vector<std::pair<int, int>> possibleMovesVector (4, std::make_pair(-1, -1));
	
	// Up
	if (i-1 >= 0)
		possibleMovesVector.at(0) = std::make_pair(i-1, y);

	// Bottom
	if (i+1 < islandVector.size())
		possibleMovesVector.at(1) = std::make_pair(i+1, y);
	
	// Right
	if (y+1 < islandVector.at(0).size())
		possibleMovesVector.at(2) = std::make_pair(i, y+1);
	
	// Left
	if (y-1 >= 0)
		possibleMovesVector.at(3) = std::make_pair(i, y-1);
	
	return possibleMovesVector;
}

std::vector<int> Island::possibleActionFromState(int state) {
	std::vector<int> possibleActionFromStateVector;
	std::pair<int, int> initialPosition = stateToPosition(state);
	
	// North
	if (initialPosition.first-1 >= 0)
		possibleActionFromStateVector.push_back(0);

	// South
	if (initialPosition.first+1 < islandVector.size())
		possibleActionFromStateVector.push_back(1);
	
	// East
	if (initialPosition.second+1 < islandVector.at(0).size())
		possibleActionFromStateVector.push_back(2);
	
	// West
	if (initialPosition.second-1 >= 0)
		possibleActionFromStateVector.push_back(3);
	
	return possibleActionFromStateVector;
}

std::vector<std::pair<int, int>> Island::onlyPossibleMoves (int i, int y) {
	std::vector<std::pair<int, int>> possibleMovesVector;
	
	// Up
	if (i-1 > 0)
		possibleMovesVector.push_back(std::make_pair(i-1, y));

	// Bottom
	if (i+1 < islandVector.size())
		possibleMovesVector.push_back(std::make_pair(i+1, y));
	
	// Right
	if (y+1 < islandVector.at(0).size())
		possibleMovesVector.push_back(std::make_pair(i, y+1));
	
	// Left
	if (y-1 > 0)
		possibleMovesVector.push_back(std::make_pair(i, y-1));
	
	return possibleMovesVector;
}

std::pair<int, int> Island::pickRandomMove(std::vector<std::pair<int, int>> possibleMovesVector) {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distribution(0, possibleMovesVector.size()-1);
	return possibleMovesVector.at(distribution(generator));
}

int Island::pickRandomAction(std::vector<int> possibleActionVector) {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distribution(0, possibleActionVector.size()-1);
	return possibleActionVector.at(distribution(generator));
}

std::vector<std::vector<int>> Island::stateScoreMatrix() {
	int sizeMatrix = (islandVector.size()*islandVector.at(0).size());
	std::vector<std::vector<int>> stateMatrix (sizeMatrix, std::vector<int>(4, -1));
	
	for (unsigned i=0; i<islandVector.size(); i++) {
		for (unsigned j=0; j<islandVector.at(i).size(); j++) {
			int stateFromMatrix = positionToState(std::make_pair(i, j));
			std::vector<std::pair<int, int>> possibleMoveVector = possibleMoves(i, j);
			for (unsigned coordinates=0; coordinates<possibleMoveVector.size(); coordinates++) {
				if (possibleMoveVector.at(coordinates).first != -1 && possibleMoveVector.at(coordinates).second != -1) {
					stateMatrix.at(stateFromMatrix).at(coordinates) = islandVector.at(possibleMoveVector.at(coordinates).first)
						.at(possibleMoveVector.at(coordinates).second);
				}
			}
		}
	}

	return stateMatrix;
}

void Island::printStateMatrix() {
	std::vector<std::vector<int>> matrix = stateScoreMatrix();
	for (unsigned i=0; i<matrix.size(); i++) {
		std::cout<<"s["<<i<<"]\tNorth\tSouth\tEast\tWest"<<std::endl;
		std::cout<<"\t";
		for (unsigned j=0; j<matrix.at(i).size(); j++) {
			std::cout<<matrix.at(i).at(j)<<"\t";
		}
		std::cout<<std::endl;
	}
}

int Island::stateActionToNewState(int state, int action) {
	std::pair<int, int> initialPosition = stateToPosition(state);
	switch (action) {
		// North
		case 0:
			return positionToState((std::make_pair(initialPosition.first-1, initialPosition.second)));
			break;

		// South
		case 1:
			return positionToState((std::make_pair(initialPosition.first+1, initialPosition.second)));
			break;
			
		// East
		case 2:
			return positionToState((std::make_pair(initialPosition.first, initialPosition.second+1)));
			break;
			
		// West
		case 3:
			return positionToState((std::make_pair(initialPosition.first, initialPosition.second-1)));
			break;
		
		// Impossible
		default:
			return -1;
	}
}

std::pair<int, int> Island::stateToPosition(int stateInMatrix){
	return std::make_pair((int)(stateInMatrix/islandVector.at(0).size()), stateInMatrix%islandVector.at(0).size());
};

int Island::positionToState(std::pair<int, int> positionFromIslandVector){
	return positionFromIslandVector.first*islandVector.at(0).size()+positionFromIslandVector.second;
};

