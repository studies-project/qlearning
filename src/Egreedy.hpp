#pragma once

#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include "Color.hpp"
#include "Island.hpp"

class Egreedy {
	private:
		Island island;
		double e;
	public:
		Egreedy(Island island, double e);
		std::pair<bool, int> run();
		std::vector<std::pair<int, int>> possibleMoves(int i, int y);
		std::pair<int, int> pickRandomMove(std::vector<std::pair<int, int>> possibleMovesVector);
		std::pair<int, int> pickBestMove(std::vector<std::pair<int, int>> possibleMovesVector);
};