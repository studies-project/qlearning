#pragma once

#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include "Color.hpp"

class Island {
	public:
		std::vector<std::vector<int>> islandVector;
		Island(int sizeX, int sizeY, int numberBottleRhum);
		void printIsland();
		std::vector<std::pair<int, int>> possibleMoves(int i, int y);
		std::vector<std::pair<int, int>> onlyPossibleMoves (int i, int y);
		std::vector<int> possibleActionFromState(int state);
		std::pair<int, int> pickRandomMove(std::vector<std::pair<int, int>> possibleMovesVector);
		int pickRandomAction(std::vector<int> possibleActionVector);
		std::vector<std::vector<int>> stateScoreMatrix();
		void printStateMatrix();
		std::pair<int, int> stateToPosition(int stateFromMatrix);
		int positionToState(std::pair<int, int> positionFromIslandVector);
		int stateActionToNewState(int state, int action);
};