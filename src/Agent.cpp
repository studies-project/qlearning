#include "Agent.hpp"

#define NBSTEPMAX 500

Agent::Agent(Island island, double gamma) : island(island), gamma(gamma) {
	rMatrix = island.stateScoreMatrix();
	qMatrix = std::vector<std::vector<int>> (rMatrix.size(), std::vector<int>(4, 0));
}

int Agent::Q(int s, int a) {
	// We search for a new state and the next actions possibles and search for the maximum score.
	int nextState = island.stateActionToNewState(s, a);
	std::vector<int> actionVector = island.possibleActionFromState(nextState);
	int score = -1;
	for (unsigned i=0; i<actionVector.size(); i++) {
		if (score < qMatrix.at(nextState).at(actionVector.at(i)))
			score = qMatrix.at(nextState).at(actionVector.at(i));
	}
	qMatrix.at(s).at(a) = rMatrix.at(s).at(a) + gamma * score;
	return nextState;
}

int Agent::pickRandomState() {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distribution(0, rMatrix.size()-1);
	return distribution(generator);
}

// We search if we found the treasure
bool Agent::isStateFinal(int s) {
	std::pair<int, int> coordinates = island.stateToPosition(s);
	return (island.islandVector.at(coordinates.first).at(coordinates.second) == 10);
}

void Agent::runEpisode() {
	int currentState = pickRandomState();
	do {
		int randomAction = island.pickRandomAction(island.possibleActionFromState(currentState));
		currentState = Q(currentState, randomAction);
	} while (!isStateFinal(currentState));
}

void Agent::printQMatrix() {
	for (unsigned i=0; i<qMatrix.size(); i++) {
		std::cout<<"s["<<i<<"]\tNorth\tSouth\tEast\tWest"<<std::endl;
		std::cout<<"\t";
		for (unsigned j=0; j<qMatrix.at(i).size(); j++) {
			std::cout<<qMatrix.at(i).at(j)<<"\t";
		}
		std::cout<<std::endl;
	}
}

void Agent::run(int episodes) {
	for (int i=0; i<episodes; i++)
		runEpisode();
}

int Agent::searchPathFromState(int state) {
	int currentState = state, step = 0;
	do {
		currentState = pickMaxState(currentState);
		step++;
	} while (!isStateFinal(currentState) && step<NBSTEPMAX);
	return step;
}

int Agent::pickMaxState(int currentState) {
	int score = -1, bestAction;
	for (unsigned j=0; j<qMatrix.at(currentState).size(); j++) {
		if (score < qMatrix.at(currentState).at(j)) {
			score = qMatrix.at(currentState).at(j);
			bestAction = j;
		}
	}

	return island.stateActionToNewState(currentState, bestAction);
}