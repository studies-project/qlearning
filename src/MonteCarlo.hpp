#pragma once

#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include "Color.hpp"
#include "Island.hpp"

class MonteCarlo {
	private:
		Island island;
	public:
		MonteCarlo(Island island);
		int randomStep(int x, int y, int nbStepsMax);
		int run();
};