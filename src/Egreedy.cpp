#include "Egreedy.hpp"

#define NBSTEPMAX 1000

Egreedy::Egreedy(Island island, double e) : island(island), e(e) {}

std::pair<bool, int> Egreedy::run() {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distributionX(0, island.islandVector.size()-1);
	std::uniform_int_distribution<int> distributionY(0, island.islandVector.at(0).size()-1);
	std::pair<int, int> position (distributionX(generator), distributionY(generator));
	int step, sum=0;
	bool foundTreasure=false;

	for (step=0; step<NBSTEPMAX; step++) {
		if (island.islandVector.at(position.first).at(position.second) == 10) {
			foundTreasure=true;
			break;
		}

		std::uniform_real_distribution<double> distribution2(0.0, 1.0);
		double percent = distribution2(generator);
		if (percent <= 1-e)
			position = pickRandomMove(island.onlyPossibleMoves(position.first, position.second));
		else
			position = pickBestMove(island.onlyPossibleMoves(position.first, position.second));

		sum += island.islandVector.at(position.first).at(position.second);
	}

	// std::cout<<"Sum of score : "<<sum<<std::endl;

	return std::make_pair(foundTreasure, step);
}

std::pair<int, int> Egreedy::pickRandomMove(std::vector<std::pair<int, int>> possibleMovesVector) {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distribution(0, possibleMovesVector.size()-1);
	return possibleMovesVector.at(distribution(generator));
}

std::pair<int, int> Egreedy::pickBestMove(std::vector<std::pair<int, int>> possibleMovesVector) {
	int bestScore=0;
	std::pair<int, int> bestMove;
	for (auto element : possibleMovesVector) {
		if (island.islandVector.at(element.first).at(element.second) > bestScore)
			bestScore = island.islandVector.at(element.first).at(element.second);
			bestMove = element;
		}

	return bestMove;
}