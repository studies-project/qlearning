#include <iostream>
#include <chrono>
#include <functional>

#include "Island.hpp"
#include "Egreedy.hpp"
#include "Agent.hpp"
#include "MonteCarlo.hpp"

#define NUMBEREXPERIENCE	10
#define SIZEX				5
#define SIZEY				7
#define NUMBERBOTTLE		4

using namespace std;

pair<double, double> calculateMean(default_random_engine generator, 
	function<pair<int, double> (default_random_engine)> functionName) {
	
	vector<pair<int, double>> results;
	for (int i=0; i<NUMBEREXPERIENCE; i++)
		results.push_back(functionName(generator));

	double stepSum=0, timeSum=0;
	for (auto element : results) {
		stepSum+=element.first;
		timeSum+=element.second;
	}

	return make_pair(stepSum/NUMBEREXPERIENCE, timeSum/NUMBEREXPERIENCE);
}

pair<int, double> qLearningAux(default_random_engine generator) {
	auto start = chrono::system_clock::now();
	Island island(SIZEX, SIZEY, NUMBERBOTTLE);
	Agent agent(island, 0.9);
	agent.run(100);
	uniform_int_distribution<int> distribution(0, agent.qMatrix.size()-1);
	int step = agent.searchPathFromState(distribution(generator));
	auto end = chrono::system_clock::now();
	return make_pair(step, ((chrono::duration<double>)(end-start)).count());
}

pair<int, double> eGreedyAux(default_random_engine generator) {
	auto start = chrono::system_clock::now();
	Island island(SIZEX, SIZEY, NUMBERBOTTLE);
	Egreedy greedy(island, 0.1);
	int step = (greedy.run()).second;
	auto end = chrono::system_clock::now();
	return make_pair(step, ((chrono::duration<double>)(end-start)).count());
}

pair<int, double> monteCarloAux(default_random_engine generator) {
	auto start = chrono::system_clock::now();
	Island island(SIZEX, SIZEY, NUMBERBOTTLE);
	MonteCarlo monteCarlo(island);
	int step = monteCarlo.run();
	auto end = chrono::system_clock::now();
	return make_pair(step, ((chrono::duration<double>)(end-start)).count());
}

int main() {
	default_random_engine generator(random_device{}());
	
	cout<<"Numbers of experience : "<<NUMBEREXPERIENCE<<"\tEnvironnement size "<<SIZEX<<"x"<<SIZEY<<endl;
	pair<double, double> qLearningResults = calculateMean(generator, qLearningAux);
	cout<<"\t"<<"Q Learning :\t"	<<qLearningResults.first<<" steps\t"	<<qLearningResults.second<<"s per run."<<endl;
	pair<double, double> eGreedyResults = calculateMean(generator, eGreedyAux);
	cout<<"\t"<<"E Greedy :\t"		<<eGreedyResults.first<<" steps\t"		<<eGreedyResults.second<<"s per run."<<endl;
	pair<double, double> monteCarloResults = calculateMean(generator, monteCarloAux);
	cout<<"\t"<<"Monte Carlo :\t"	<<monteCarloResults.first<<" steps\t"	<<monteCarloResults.second<<"s per run."<<endl;
	return 0;
}