#pragma once

#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include "Color.hpp"
#include "Island.hpp"

class Agent {
	public:
		Island island;
		std::vector<std::vector<int>> rMatrix;
		std::vector<std::vector<int>> qMatrix;
		double gamma;
		Agent(Island island, double gamma);
		int Q(int s, int a);
		int pickRandomState();
		bool isStateFinal(int s);
		void runEpisode();
		void run(int episodes);
		void printQMatrix();
		int searchPathFromState(int state);
		int pickMaxState(int currentState);
};