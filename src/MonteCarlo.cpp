#include "MonteCarlo.hpp"

#define NBSTEPMAX 500

MonteCarlo::MonteCarlo(Island island) : island(island) { }

int MonteCarlo::randomStep(int x, int y, int nbStepsMax) {
	int step;
	std::pair<int, int> position(x, y);

	for (step=0; step<nbStepsMax; step++) {
		position = island.pickRandomMove(island.onlyPossibleMoves(position.first, position.second));
		if (island.islandVector.at(position.first).at(position.second) == 10)
			return nbStepsMax-step;
	}

	return 0;
}

int MonteCarlo::run() {
	std::default_random_engine generator(std::random_device{}());
	std::uniform_int_distribution<int> distributionX(0, island.islandVector.size()-1);
	std::uniform_int_distribution<int> distributionY(0, island.islandVector.at(0).size()-1);
	std::pair<int, int> position (distributionX(generator), distributionY(generator));
	std::pair<int, int> bestMove;
	int step=0;
	do {
		int score=0;
		std::vector<std::pair<int, int>> possibleMoves = island.onlyPossibleMoves(position.first, position.second);
		for (auto element : possibleMoves) {
			int newScore = randomStep(element.first, element.second, NBSTEPMAX);
			if (newScore > score) {
				score = newScore;
				bestMove = element;
			}
		}
		position = bestMove;
		step++;
	} while (island.islandVector.at(position.first).at(position.second) != 10);
	return step;
}