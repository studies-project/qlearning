# QLearning

Graduate project of QLearning.

## Compilation

```sh
mkdir build
cd build
cmake ..
make
```

## Use

```sh
./main
```

The program seems to sometimes be stucked in the Q Learning "searchPathFromState" loop function. I'm searching the bug.

## Output

```sh
Numbers of experience : 10      Environnement size 3x5
		Q Learning :    2.3 steps       0.00192757s per run.
		E Greedy :      407.2 steps     0.000215889s per run.
		Monte Carlo :   6.6 steps       0.00221975s per run.
```

Here we see how the Q Learning is the best one, in fewer steps and in less time per run he finds the treasure. The Monte Carlo is a little bit slower and needs more steps to find it, where the E Greedy is the slowest because he needs much more steps.